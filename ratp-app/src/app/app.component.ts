import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'ratp-app';
  commerces = {};

  // default value
  defaultValue = {
    facet : "code_postal",
    libelle : "tco_libelle",
    sort : "code_postal",
    ville : "ville",
    rows : "1000",
    url : "https://data.ratp.fr/api/records/1.0/search/?dataset=liste-des-commerces-de-proximite-agrees-ratp"
  };

  tri = {
    lib : false,
    cp : false,
    ville : true
  };

  url = "";
  urlbackup;
  constructor(private http: HttpClient){

  }

  ngOnInit(): void {
    this.url = this.build_default_search();
    this.http.get(this.url).subscribe(data => {
        this.commerces = data;
    });
  }

  search(lib, ville, cp) {
    this.url = this.build_default_search();
    if (lib !== "") {
      this.url += "&refine.tco_libelle=" + lib;
    }
    if (ville !== "") {
      this.url += "&refine.ville=" + ville;
    }
    if (cp !== "") {
      this.url += "&refine.code_postal=" + cp;
    }

    this.http.get(this.url).subscribe(data => {
      this.commerces = data;
    });
  }

  build_search(rows, libelle, facet, ville) {
    return this.defaultValue.url+"&rows="+rows+"&facet="+libelle+"&facet="+ville+"&facet="+facet;
  }

  build_default_search() {
    return this.url = this.build_search(
        this.defaultValue.rows,
        this.defaultValue.libelle,
        this.defaultValue.facet,
        this.defaultValue.ville
    );
  }

  change_tri(val) {
    this.urlbackup = this.url;
    this.tri.cp = !this.tri.cp;
    if (this.tri.cp) {
      this.url += "&sort=code_postal";
    } else {
      this.url += "&sort=-code_postal";
    }
    this.http.get(this.url).subscribe(data => {
      this.commerces = data;
    });
    this.url = this.urlbackup;
  }

}

